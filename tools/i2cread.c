/*
    i2cget.c - A user-space program to read an I2C register.
    Copyright (C) 2005-2010  Jean Delvare <khali@linux-fr.org>

    Based on i2cset.c:
    Copyright (C) 2001-2003  Frodo Looijaard <frodol@dds.nl>, and
                             Mark D. Studebaker <mdsxyz123@yahoo.com>
    Copyright (C) 2004-2005  Jean Delvare <khali@linux-fr.org>

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
    MA 02110-1301 USA.
*/

#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <linux/i2c-dev.h>
#include "i2cbusses.h"
#include "util.h"
#include "../version.h"

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main(int argc, char *argv[])
{
    char* filename = "/dev/i2c-2";
    int addr = 0x64;
    uint8_t buf[32];
	int file;

    //read command num
    uint16_t cmd = (uint16_t)strtol(argv[1], NULL, 0);
    buf[0] = 0xFF & (cmd >> 8);
    buf[1] = 0xFF & cmd;


    if((file = open(filename, O_RDWR)) < 0) {
        perror("Failed to open i2c bus");
        exit(1);
    }

    if(ioctl(file, I2C_SLAVE, addr) < 0) {
        printf("failed to acquire bus access and/or talk to slave.\n");
        exit(1);
    }

    if(write(file, buf, 2) != 2) {
        printf("failed to write to i2c bus.\n");
        exit(1);
    }

    if(read(file, buf, 2) != 2) {
        printf("failed to read from i2c bus.\n");
        exit(1);
    }

    printf("read: 0x%x 0x%x\n", buf[0], buf[1]);

	exit(0);
}
